#!/bin/sh

distdir=`cd $(dirname $0) && pwd`
logging="${distdir}/log/setup.log"

cd $distdir

date +%Y-%m-%dT%H:%M:%S+09:00                   >>$logging 2>&1

cd ./app
git clone git://github.com/typester/nim.git nim >>$logging 2>&1
cd ../

cpanm -l extlib < requires.txt                  >>$logging 2>&1


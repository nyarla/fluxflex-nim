
data_dir:      ./entries
templates_dir: ./templates
site_dir:      ./output

plugins:
  - module: Entry::File
  - module: Markdown
  - module: Render::Entry

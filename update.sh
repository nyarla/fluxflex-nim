
#!/bin/sh

distdir=`cd $(dirname $0) && pwd`
logging="${distdir}/log/deploy.log"

cd $distdir

date +%Y-%m-%dT%H:%M:%S+09:00                   >>$logging 2>&1

cd ./app

if [ ! -d nim ]; then
    git clone git://github.com/typester/nim.git nim >>$logging 2>&1
fi;

if [ -d nim ]; then
    cd nim && git pull origin master && cd ..       >>$logging 2>&1
fi;

cd ../

cpanm -l extlib < requires.txt                  >>$logging 2>&1

if [ -d output ]; then
    rm -rf output;
fi;
 
env PERL5LIB=./extlib/lib/perl5:./extlib/lib/perl5/x86_64-linux-gnu-thread-multi:./app/nim/lib perl app/nim/bin/nim >>$logging 2>&1

if [ ! -d ./output/.log ]; then
    mkdir -p ./output/.log;
fi;

cp -R ./log/*.log ./output/.log/

rsync -avz --delete ./output/ ./public_html >>$logging 2>&1
